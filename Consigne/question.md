# Employé du mois

Soit le fichier  `employee.xlsx`.

On veut virer les employées en trop et récompenser les employés productifs de nos usines.

Pour ce faire on vous demande de réaliser un dossier d’analyse à partir des données du mois de janvier.

Pour chaque question posée vous devez fournir la réponse, une justification de la réponse et un visuel (graphique, schema, courbe etc.).

Vous produirez un PDF avec toutes vos réponses.

On veut un script qui permet de produire les réponses aux mêmes questions (première partie) pour le fichier d’un autre mois.

## Questions

1. Liste des employées qui ont menti sur leurs heures supplémentaires et liste des employés ayant eu au moins une journée d’absence.
Il ne doivent pas être pris en compte pour les questions de la première partie.

2. liste des 10 employées qui coûtent le plus cher (qui touche le plus gros salaire).
Sachant que :

- Le salaire horaire de base de tous les employés dépend de leur département :

| Département | Salaire de base |
|-------------|-----------------|
| Produciton  | 12              |
| Management  | 50              |
| Support     | 20              |
| RD          | 30              |

- Une prime quotidienne est accordée pour chaque jour de travail en fonction de l’usine.

| Usine | Prime |
|-------|-------|
| A     | 15    |
| B     | 10    |
| C     | 20    |

- Les heures supplémentaires déclarées sont payé 125% du salaire de base.

3. liste des 10 employés qui ont travaillé le plus d’heure et leur salaire.
4. liste des 10 employés qui ont travaillé le moins d’heure et leur salaire.
5. liste des 10 employés qui ont été le plus de temps en pause.
6. liste des employées qui ont fait le plus d’heures supplémentaires.
7. répartition des jours de travail en fonction des usines.
8. répartition des heures de travail en fonction des usines.
9. liste des 10 employés qui ont fait le plus de R&D par usine.
10. liste des 10 employés qui ont pris plus de 50 minutes de pause dans chaque usine.
11. liste des employés qui ont le plus travaillé par usine.
12. Combien d’heure supplémentaires ont été travaillées ?
13. Y a-t-il un lien entre heures supplémentaires et temps de pause ?
14. Y a-t-il un lien entre heures supplémentaires et heures travaillées ?
15. Y a-t-il un lien entre heures travaillées et temps de pause ?

## Deuxième partie : concernant l’incendie

16. Où a eu lieu l’incendie ?
17. Quand a eu lieu l’incendie ?
18. Pouvait-on prévoir l’incendie ?
19. Qui a mis le feu ?
20. Quel a été l’influence de l’incendie sur la production ?

## Question bonus

21. Qui est l’employé du mois ?