from Utiles.utile_extract import *
from Utiles.utile_CSV import *

import time

def importation_employee(path):
    # Initialisation du fichier Excel
    xls = pd.ExcelFile(path)
    # Récupération du 1er sheet Employee
    dataframe_employee = extract_sheet_excel(xls, 0, header=0)
    # Modification dataframe
    dataframe_employee.drop(columns=dataframe_employee.columns[3:5], inplace=True)
    dataframe_employee.rename({'employee_id': 'id'}, axis=1, inplace=True)
    dataframe_employee.set_index(['id'], inplace=True)   
    return dataframe_employee

def importation_pointage(path):
    # Initialisation du fichier Excel
    xls = pd.ExcelFile(path)
    # Récupération des autres sheets Journée_Travail par Date
    list_dico_dataframe = extract_all_same_sheet_excel(xls)
    # Modification et Affichage dataframe
    for dico in list_dico_dataframe:
        dico["dataframe"].set_index(['id'], inplace=True)   
    return list_dico_dataframe

def exportation_csv(dataframe, name_file):
    path = 'DATA/Export/'
    dataframe.to_csv(f"{path}{name_file}.csv")

def main(path, day_off):
    # Récupération des datas
    df_employee = importation_employee(path)
    # Affichage dataframe
    # print("------Columns:------", df_employee.columns)
    # print("------Employee:------\n", df_employee)
    # Récupération des datas
    L_Dico_pointage = importation_pointage(path)
    List_all_pointage = []
    # Insertion dataframe dans dictionnaire
    for dico in L_Dico_pointage:
        dico["dataframe"]["date"] = dico["sheet"]
        # print("------Columns:", dico["dataframe"].columns)
        # print("------Jounée de Travail:", dico["sheet"],"------")
        if dico["sheet"] in day_off:
            jour_ferie(dico)
        conversion_time_by_minute(dico)
        # print("------Dataframe:------\n", dico["dataframe"])
        if dico["sheet"] not in day_off:
            List_all_pointage.append(dico["dataframe"])
    # Fusion de tous les df pointages du dico
    df_all_pointage = concat_dataframe(List_all_pointage)
    # Fusion du df pointage et employee
    df_all_pointage = pd.merge(df_all_pointage, df_employee, on="id")
    # Modification des colonnes du dataframe
    df_all_pointage = change_columns(df_all_pointage)
    # Export du dataframe vers CSV
    exportation_csv(df_all_pointage, "pointage_complet")
    exportation_csv(
        df_all_pointage[['employee_name', 'department', 'location', 'date', 
        'shift_start_minutes', 'shift_end_minutes', 'temps_de_travail_minutes',
        'overtime_minutes', 'break_duration', 'tricheurs_minutes']], 
        "pointage")


if __name__ == "__main__":
    # Saisir le chemin du fichier (Mois) à analyser
    path = 'DATA/employee.xlsx'
    # Remplir les jours non travaillés au format "jj-mm-yyyy"
    day_off = ["28-01-2024"]
    # Lancement du script
    print("------Lancement du script------")
    start = time.time()
    main(path, day_off)
    end = time.time()
    elapsed = end - start
    print(f'Temps d\'exécution : {elapsed:.2}ms')
    print("------Script terminé------")
