import pandas as pd
import time
import plotly.express as px

from Utiles.utile_question import *
from Utiles.utile_export_PDF import *
from Utiles.text_for_PDF import * 

def question_1(path, dataframe): # pas de figure
    print("-"*25, "Question 1", "-"*25)
    nb_jours_in_month = 31
    nb_jours_holidays = 1
    nb_jours = nb_jours_in_month - nb_jours_holidays
    # Absent:
        # Calcul des jours travailler par employée
    df_day_employe = dataframe.value_counts('employee_name')
        # Transforme le df en dico pour map un autre df
    df_day_by_name = df_day_employe.reset_index()
    liste_day_by_name = df_day_by_name.to_dict('split')["data"]
    dico_day_by_name = list_of_list_to_dict(liste_day_by_name)
    dataframe["nb_jours_travaillé"] = dataframe["employee_name"].map(dico_day_by_name)
        # Liste des personnes sans absence
    serie_without_absent = df_day_employe[df_day_employe >= nb_jours]
    df_without_absent = serie_without_absent.reset_index()
        # Liste des personnes avec absence
    serie_absent = df_day_employe[df_day_employe < nb_jours]
    df_absent = serie_absent.reset_index()
        # Comparaison des personnes avec la liste des sans_absence
    df_employee = compare_df_asymetrique(dataframe, df_without_absent, ("employee_name", "employee_name"))
    df_employee = df_employee.sort_values(by="tricheurs_minutes", ascending = False)
    # Heure Sup erronée:
    df_bad_employee = df_employee[df_employee["tricheurs_minutes"] < 0]
    # Heure Sup non-erronée:
    df_good_employee = df_employee[df_employee["tricheurs_minutes"] > 0]
    # Réponse à la question:
        # print(df_absent)
        # print(df_bad_employee)
        # print(df_without_absent)
    dataframe_30er_good_employee = df_good_employee.nlargest(30, 'tricheurs_minutes')
        # print(dataframe_10er_good_employee)
    # Export question vers PDF
    question_to_pdf(path, q01, dataframe_30er_good_employee)
    return dataframe_30er_good_employee

def question_2(path, dataframe): # pas de figure
    print("-"*25, "Question 2", "-"*25)
    # Salaire de base
    salaire = {"Production": 12, "Management": 50, "Support": 20, "RD": 30}
    prime = {"Usine A": 15, "Usine B": 10, "Usine C": 20}
    # Insertion du salaire de base en fonction du departement
    dataframe["base"] = dataframe["department"].map(salaire)
    dataframe["base_overtime"] = dataframe["department"].map(salaire)
    dataframe["base_overtime"] = dataframe["base_overtime"].apply(lambda x: int(x) * 1.25)
    # Insertion de la prime en fonction du departement
    dataframe["prime"] = dataframe["location"].map(prime)
    # Calcul du temps en heure
    dataframe["temps_effectif_minutes"] = dataframe["temps_de_travail_minutes"] - dataframe["break_duration"]
    dataframe["temps_effectif_hours"] = dataframe["temps_effectif_minutes"].apply(lambda x: int(x) / 60)
    # Calcul du temps total travaillé en heure
    dataframe["temps_hours&overtime"] = dataframe["temps_effectif_hours"] + dataframe["overtime_hours"]
    # Calcul du salaire en fonction du temps de travail
    dataframe["salaires_hours"] = dataframe["temps_effectif_hours"] * dataframe["base"]
    dataframe["salaires_overtime"] = dataframe["overtime_hours"] * dataframe["base_overtime"]
    # Calcul du salaire par jour
    dataframe["salary_day"] = dataframe["salaires_hours"] + dataframe["salaires_overtime"]
    dataframe["salary_day"] = dataframe["salary_day"].apply(lambda x: np.round(x, 2))
    # Calcul du salaire par mois
    dataframe_salaire = dataframe[['employee_name', 'salary_day']].groupby(['employee_name']).sum().sort_values(by='salary_day')
    salaire_by_mois: dict = dataframe_salaire.to_dict()["salary_day"]
    # Insertion du salaire mensuel par employee
    dataframe["salary_month"] = dataframe["employee_name"].map(salaire_by_mois) 
    # Trie du dataframe par salaire
    dataframe_salaire.rename(columns={'salary_day': 'salary_month'}, inplace=True)
    dataframe_10er_salaire = dataframe_salaire.nlargest(10, 'salary_month')
    # Réponse à la question:
    # print(dataframe_10er_salaire)
    # Export question vers PDF
    question_to_pdf(path, q02, dataframe_10er_salaire)
    return dataframe

def question_3(path, dataframe): # pas de figure
    print("-"*25, "Question 3", "-"*25)
    dataframe_10er_hours = dataframe.nlargest(10, 'temps_hours&overtime')
    # Réponse à la question:
    # print(dataframe_10er_hours[["id", "employee_name", "temps_hours&overtime", "salary_month"]])
    # Export question vers PDF
    question_to_pdf(path, q03, dataframe_10er_hours[["id", "employee_name", "temps_hours&overtime", "salary_month"]])

def question_4(path, dataframe): # pas de figure
    print("-"*25, "Question 4", "-"*25)
    dataframe_10last_hours = dataframe.nsmallest(10, 'temps_hours&overtime')
    # Réponse à la question:
    # print(dataframe_10last_hours[["id", "employee_name", "temps_hours&overtime", "salary_month"]])
    # Export question vers PDF
    question_to_pdf(path, q04, dataframe_10last_hours[["id", "employee_name", "temps_hours&overtime", "salary_month"]])

def question_5(path, dataframe): # pas de figure
    print("-"*25, "Question 5", "-"*25)
    dataframe_10er_break = dataframe.nlargest(10, 'break_duration')
    # Réponse à la question:
    # print(dataframe_10er_break[["id", "employee_name", "break_duration"]])
    # Export question vers PDF
    question_to_pdf(path, q05, dataframe_10er_break[["id", "employee_name", "break_duration"]])

def question_6(path, dataframe): # pas de figure
    print("-"*25, "Question 6", "-"*25)
    dataframe_by_overtime = dataframe.sort_values(by="overtime_hours", ascending = False)
    # Réponse à la question:
    # print(dataframe_by_overtime[["id", "employee_name", "overtime_hours"]])
    # Export question vers PDF
    question_to_pdf(path, q06, dataframe_by_overtime[["id", "employee_name", "overtime_hours"]])

def question_7(path, dataframe):
    print("-"*25, "Question 7", "-"*25)
    # Création dataframe: jours travaillés en fonction des dates
        # Fonction to_datetime
    dataframe['date'] = pd.to_datetime(dataframe['date'], dayfirst=True)
        # Jour du mois 
    dataframe["number_day_date"] = dataframe['date'].dt.day
    dataframe["day_date"] = dataframe['date'].dt.strftime('%A')
    # print(dataframe[["id", "employee_name", "location", "date", "number_day_date", "day_date"]])
    # Répartition par Usines
    dataframe_by_work_day = dataframe[["location", "day_date"]]
    dataframe_by_work_day = dataframe_by_work_day.groupby(by=["day_date"])["location"].value_counts().unstack()
    # dataframe_by_work_day = dataframe_by_work_day.assign(total=dataframe_by_work_day.sum(1))
    dataframe_by_work_day = dataframe_by_work_day.fillna(0)
    # Réponse à la question:
    # print(dataframe_by_work_day)
    fig = px.bar(dataframe_by_work_day)
    # fig.show()
    # Export question vers PDF
    question_to_pdf(path, q07, dataframe_by_work_day, graphique=fig)
    return dataframe

def question_8(path, dataframe):
    print("-"*25, "Question 8", "-"*25)
    def dict_labels(heure_debut, heure_fin):
        minutes_list = [x * 60 for x in range(heure_debut, heure_fin)]
        hours_list = []
        for x in range(heure_debut, heure_fin):
            if x >= 24:
                x = x - 24
            if x < 10:
                dixaine_hours = "0"
            else:
                dixaine_hours = ""
            hours_x = str(x)
            chaine_hours = dixaine_hours + hours_x + ":00"
            hours_list.append(chaine_hours)
        return dict(zip(minutes_list, hours_list))
    def bin_helper(code_dict):
        break_points = [0] + sorted(code_dict) #0 added for lower bound on binning
        labels = []
        for value in sorted(code_dict):
            if value - 60 in code_dict.keys():
                next_value = value - 60
            else:
                next_value = 1440
            # tuple_value = (code_dict[next_value], code_dict[value]) # plage horaire
            start_value = code_dict[next_value] # heure de début uniquement
            labels.append(start_value)
        return break_points, labels
    # Création dataframe: plage d'horaire en fonction des shifts
    conversion_dict = dict_labels(1, 25) # heure_debut, heure_fin
    bins, labels = bin_helper(conversion_dict)
    dataframe["shift_start_range"] = pd.cut(x=dataframe["shift_start_minutes"], bins=bins, labels=labels)
    dataframe["shift_end_range"] = pd.cut(x=dataframe["shift_end_minutes"], bins=bins, labels=labels)
    dataframe['shift_range'] = dataframe['shift_start_range'].astype(str) + "_to_" + dataframe['shift_end_range'].astype(str)
    # print(dataframe[["id", "employee_name", "shift_start_time", "shift_end_time", "shift_start_range", "shift_end_range", "shift_range"]])
    # Répartition par Usines
    dataframe_by_hours = dataframe[["location", "shift_range"]]
    dataframe_by_hours = dataframe_by_hours.groupby(by=["shift_range"])["location"].value_counts().unstack()
    # dataframe_by_hours = dataframe_by_hours.assign(total=dataframe_by_hours.sum(1))
    dataframe_by_hours = dataframe_by_hours.fillna(0)
    # Réponse à la question:
    # print(dataframe_by_hours)
    fig = px.bar(dataframe_by_hours)
    # fig.show()
    # Export question vers PDF
    question_to_pdf(path, q08, dataframe_by_hours, graphique=fig)
    return dataframe

def question_9(path, dataframe): # pas de figure
    print("-"*25, "Question 9", "-"*25)
    # Création dataframe: jours travaillés en fonction du département RD
    # print(dataframe[["id", "employee_name", "location", "department"]])
    df_RD = dataframe[dataframe["department"] == "RD"]
    # print(df_RD[["id", "employee_name", "location", "department"]])
    # Répartition par Usines
    dataframe_by_RD = df_RD[["id", "employee_name", "location", "department"]]
    dataframe_by_RD = dataframe_by_RD.groupby(by=["id", "employee_name", "department"])["location"].value_counts().unstack()
    dataframe_by_RD = dataframe_by_RD.assign(total=dataframe_by_RD.sum(1)).fillna(0)
    # print(dataframe_by_RD)
    # Réponse à la question:
    dataframe_10er_RD = dataframe_by_RD.nlargest(10, 'total')
    # print(dataframe_10er_RD)
    # Export question vers PDF
    question_to_pdf(path, q09, dataframe_10er_RD)

def question_10(path, dataframe): # pas de figure
    print("-"*25, "Question 10", "-"*25)
    # Création dataframe: pauses pris
    df_break = dataframe[dataframe["break_duration"] >= 50]
    # print(df_break[["id", "employee_name", "location", "break_duration"]])
    # Répartition par Usines
    dataframe_by_break = df_break[["id", "employee_name", "location", "break_duration"]]
    dataframe_by_break = dataframe_by_break.groupby(by=["id", "employee_name", "location"]).max().unstack()
    dataframe_by_break = dataframe_by_break.assign(max=dataframe_by_break.max(axis=1)).fillna(0)
    # print(dataframe_by_break)
    # Réponse à la question:
    dataframe_by_break = dataframe_by_break.nlargest(10, 'max')
    # print(dataframe_by_break)
    # Export question vers PDF
    question_to_pdf(path, q10, dataframe_by_break)

def question_11(path, dataframe): # pas de figure
    print("-"*25, "Question 11", "-"*25)
    # Dataframe: max travaillé
    # print(dataframe[["id", "employee_name", "location", "tricheurs_minutes"]])
    # Répartition par Usines
    dataframe_max_works = dataframe[["id", "employee_name", "location", "tricheurs_minutes"]]
    dataframe_max_works = dataframe_max_works.groupby(by=["id", "employee_name", "location"]).max().unstack()
    dataframe_max_works = dataframe_max_works.assign(max=dataframe_max_works.max(axis=1)).fillna(0)
    # print(dataframe_max_works)
    # Réponse à la question:
    dataframe_max_works = dataframe_max_works.nlargest(10, 'max')
    # print(dataframe_max_works)
    # Export question vers PDF
    question_to_pdf(path, q11, dataframe_max_works)

def question_12(path, dataframe): # pas de figure
    print("-"*25, "Question 12", "-"*25)
    # print(dataframe[["id", "employee_name", "overtime_minutes"]])
    dataframe_max_works = dataframe.nlargest(10, 'overtime_minutes')
    # print(dataframe_max_works[["id", "employee_name", "overtime_minutes"]])
    dataframe_max_works.loc['Total'] = pd.Series(dataframe['overtime_minutes'].sum(), index=['overtime_minutes'])
    dataframe_max_works = dataframe_max_works[["id", "employee_name", "overtime_minutes"]].fillna('')
    # Réponse à la question:
    # print(dataframe_max_works)
    # Export question vers PDF
    question_to_pdf(path, q12, dataframe_max_works)

def question_13(path, dataframe):
    print("-"*25, "Question 13", "-"*25)
    # print(dataframe[["id", "employee_name", "overtime_minutes", "break_duration"]])
    # Export question vers PDF
    question_to_pdf(path, q13, dataframe[["id", "employee_name", "overtime_minutes", "break_duration"]])

def question_14(path, dataframe):
    print("-"*25, "Question 14", "-"*25)
    # print(dataframe[["id", "employee_name", "overtime_minutes", "tricheurs_minutes"]])
    # Export question vers PDF
    question_to_pdf(path, q14, dataframe[["id", "employee_name", "overtime_minutes", "tricheurs_minutes"]])

def question_15(path, dataframe):
    print("-"*25, "Question 15", "-"*25)
    # print(dataframe[["id", "employee_name", "tricheurs_minutes", "break_duration"]])
    # Export question vers PDF
    question_to_pdf(path, q15, dataframe[["id", "employee_name", "tricheurs_minutes", "break_duration"]])


if __name__ == "__main__":
    # Saisir le chemin du fichier (Mois) à analyser
    path = 'DATA/Export/'
    # Lancement du script
    print("------Lancement du script------")
    start = time.time()
    file_full = "pointage_complet.csv"
    df_full = pd.read_csv(path + file_full)
    df_employee_Q1 = question_1(path, df_full)
    df_employee_Q2 = question_2(path, df_employee_Q1)
    question_3(path, df_employee_Q2)
    question_4(path, df_employee_Q2)
    question_5(path, df_employee_Q2)
    question_6(path, df_employee_Q2)
    df_employee_Q7 = question_7(path, df_employee_Q2)
    df_employee_Q8 = question_8(path, df_employee_Q7)
    question_9(path, df_employee_Q8)
    question_10(path, df_employee_Q8)
    question_11(path, df_employee_Q8)
    question_12(path, df_employee_Q8)
    question_13(path, df_employee_Q8)
    question_14(path, df_employee_Q8)
    question_15(path, df_employee_Q8)
    end = time.time()
    elapsed = end - start
    print(f'Temps d\'exécution : {elapsed:.2}ms')
    print("------Script terminé------")


# pour x=1: 01:00 -> inf à 10 et inf à 24
# pour x=23: 23:00 -> sup à 10 et inf à 24
# pour x=24: 00:00 -> inf à 10 et égale à 24
# pour x=25: 01:00 -> inf à 10 et sup à 24
# pour x=47: 23:00 -> sup à 10 et sup à 24
# def dict_labels(heure_debut, heure_fin):
# minutes_list = [x * 60 for x in range(heure_debut, heure_fin)]
# hours_list = []
# for x in range(heure_debut, heure_fin):
    # if x < 24 and x < 10: # x < 10 et x < 24 --> 0 + x
    #         x = f"0{x}:00"
    #         hours_list.append(x)
    # elif x < 24 and x > 10: # x > 10 et x < 24 --> x
    #         x = f"{x}:00"
    #         hours_list.append(x)
    # elif x == 24: # x = 24 --> 0 + x - 24
    #     x = f"0{x-24}:00"
    #     hours_list.append(x)
    # elif x > 24 and x < 10: # x < 10 et x > 24 --> 0 + x
    #         x = f"0{x}:00"
    #         hours_list.append(x)
    # elif x > 24 and x > 10: # x > 10 et x > 24 --> x - 24
    #         x = f"{x-24}:00"
    #         hours_list.append(x)