import pandas as pd


def list_of_list_to_dict(list_of_list: list[list]) -> dict:
    """ Prends en entrée une liste de liste et retourne un dictionnaire
    Args:
        list_of_list (list[list]): liste de liste
    Returns:
        dict: dictionnaire
    """
    def convert(lst: list) -> dict:
        res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
        return res_dct
    dico: dict = {}
    for liste in list_of_list:
        dico = {**dico, **convert(liste)}
    return dico

def compare_df_asymetrique(dataframe, df_compare, columns: tuple):
    """ Compare deux df sur une colonne et retourne celui de base trier
    Args:
        dataframe (dataframe): dataframe à comparer
        df_compare (dataframe): dataframe de comparaison
        columns (tuple): colonne des dataframes
    Returns:
        dataframe: dataframe trier
    """
    list_new_df = []
    for index, column in enumerate(dataframe):
        if column == columns[0]:
            index_dataframe = index
    for index, column in enumerate(df_compare):
        if column == columns[1]:
            index_compare = index
    for line_compare in df_compare.values:
        for linedataframe in dataframe.values:
            if line_compare[index_compare] == linedataframe[index_dataframe]:
                list_new_df.append(linedataframe)
                continue
    new_dataframe = pd.DataFrame(list_new_df, columns=dataframe.columns)
    return new_dataframe