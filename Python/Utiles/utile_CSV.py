import numpy as np
import pandas as pd

def jour_ferie(dictionnaire):
    """public holiday or days off"""
    dataframe = dictionnaire["dataframe"]
    # Vérification si l'index est de type str
    if dataframe.index[0]  == dataframe.index.astype(str):
        # Modification de l'index
        dataframe = dataframe.reset_index()
        dataframe.iloc[0,0] = 0
        dataframe.set_index(['id'], inplace=True)
        # Mise à jour de la ligne à None
        dataframe.iloc[0,:] = None
        # Suppression des lignes
        dataframe = dataframe.dropna(how="all")
        # Mise à jour du dictionnaire
        dictionnaire["statut_df"] = "Jour Férié"
        dictionnaire["dataframe"] = dataframe

def conversion_time_by_minute(dictionnaire):
    dataframe = dictionnaire["dataframe"]
        # Version avec fonction Map
    # dataframe[["shift_start_minutes", "shift_end_minutes"]] = dataframe[["shift_start_time", "shift_end_time"]].map(temps_en_minute)
    # dataframe["hour_worked"] = dataframe["shift_end_minutes"] - dataframe["shift_start_minutes"]
    # dataframe["hour_worked_minutes"] = dataframe["hour_worked"].apply(lambda x: round((int(x) / 60), 2))
        # Version avec fonction Lambda
    # Conversion en minutes
    dataframe["shift_start_minutes"] = dataframe["shift_start_time"].apply(lambda x: temps_en_minute(str(x)))
    # Conversion en minutes
    dataframe["shift_end_minutes"] = dataframe["shift_end_time"].apply(lambda x: temps_en_minute(str(x)))
    # Calcul temps de travails
    dataframe["temps_de_travail_minutes"] = dataframe["shift_end_minutes"] - dataframe["shift_start_minutes"]
    # Conversion en minutes
    dataframe["overtime_minutes"] = dataframe["overtime_hours"].apply(lambda x: np.round(int(x) * 60))
    # Calcul du temps de travail tricher
    dataframe["tricheurs_minutes"] = dataframe['temps_de_travail_minutes'] - (dataframe["overtime_minutes"] + dataframe["break_duration"])
    dictionnaire["dataframe"] = dataframe

def concat_dataframe(liste: list):
    dataframe = pd.concat(liste)
    return dataframe

def change_columns(dataframe):
    columns = [
        'employee_name', 'department', 'location', 'date', 
        'shift_start_time', 'shift_end_time', 'overtime_hours', 
        'shift_start_minutes', 'shift_end_minutes', 'temps_de_travail_minutes',
        'overtime_minutes', 'break_duration', 'tricheurs_minutes']
    # print("Before:", dataframe.columns)
    dataframe = dataframe.reindex(columns, axis=1)
    # print("After:", dataframe.columns)
    return dataframe

def temps_en_minute(temps: str) -> int:
    """ Il me faut une fonction qui transforme ca
    "2:12" ← format H:M / en minutes : 132 ← int"""
    # je coupe ma chaine en 2 sur les : pour avoir les heures et les minutes séparées
    heures, minutes = temps.split(':')
    # Pour les heures: chacune fait 60 minutes / Pour les minutes: je cast juste en int
    return int(heures) * 60 + int(minutes)


# test
if __name__ == "__main__":
    t = "2:12"
    print(t, temps_en_minute(t))