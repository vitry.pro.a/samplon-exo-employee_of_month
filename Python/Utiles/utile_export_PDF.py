import pandas as pd
import numpy as np
from fpdf import FPDF
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def _draw_as_table(df, pagesize):
    alternating_colors = [['white'] * len(df.columns), ['lightgray'] * len(df.columns)] * len(df)
    alternating_colors = alternating_colors[:len(df)]
    fig, ax = plt.subplots(figsize=pagesize)
    ax.axis('tight')
    ax.axis('off')
    the_table = ax.table(cellText=df.values,
                        rowLabels=df.index,
                        colLabels=df.columns,
                        rowColours=['lightblue']*len(df),
                        colColours=['lightblue']*len(df.columns),
                        cellColours=alternating_colors,
                        loc='center')
    return fig


def dataframe_to_pdf(df, filename, numpages=(1, 1), pagesize=(11, 8.5)):
    with PdfPages(filename) as pdf:
        nh, nv = numpages
        rows_per_page = len(df) // nh
        cols_per_page = len(df.columns) // nv
        for i in range(0, nh):
            for j in range(0, nv):
                page = df.iloc[(i*rows_per_page):min((i+1)*rows_per_page, len(df)),
                                (j*cols_per_page):min((j+1)*cols_per_page, len(df.columns))]
                fig = _draw_as_table(page, pagesize)
                if nh > 1 or nv > 1:
                    # Add a part/page number at bottom-center of page
                    fig.text(0.5, 0.5/pagesize[0],
                                "Part-{}x{}: Page-{}".format(i+1, j+1, i*nv + j + 1),
                                ha='center', fontsize=8)
                pdf.savefig(fig, bbox_inches='tight')
                
                plt.close()

# q0? = {"question": "Question_???", "consigne": "", "variable": "", "justification": "","reponse": ""}
def question_to_pdf(path, question_dict, dataframe, graphique=None):
    # Variable de la question
    filename = path + question_dict["question"]
    type_file = ["description", "tableau", "graphique"]
    type_etape = ["question", "consigne", "variable", "justification", "reponse"]
    # Préparation du PDF
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font("Arial", "", 11)
    # Ecriture du PDF
    for ind, etape in enumerate(type_etape):
        if etape == "question":
            pdf.write(7, question_dict[etape] + " - Partie " + type_file[0])
            pdf.write(4, "\n\n")
        else:
            pdf.write(6, f"Partie " + type_etape[ind] + ":")
            pdf.write(4, "\n")
            pdf.write(4, question_dict[etape])
            pdf.write(4, "\n")
        if etape == "reponse":
            pdf.write(3, f"Voir '{type_file[1]}'")
            pdf.write(4, "\n")
            if graphique != None:
                pdf.write(3, f"Voir '{type_file[2]}'")
                pdf.write(4, "\n")
        pdf.write(4, "\n")
    # Sauvegarde du fichier
    print("Sauvegarde du fichier")
    file_desc = filename + "_" + type_file[0] + ".pdf"
    pdf.output(file_desc)
    pdf.close()
    # Création du tableau
    print("Création du tableau")
    file_tab = filename + "_"  + type_file[1] + ".pdf"
    dataframe_to_pdf(dataframe, file_tab, numpages=(1, 1), pagesize=(11, 8.5))
    pdf.close()
    # Création de la figure
    print("Création de la figure")
    file_fig = filename + "_"  + type_file[2]
    if graphique != None:
        graphique.write_image(file_fig + ".jpeg")
