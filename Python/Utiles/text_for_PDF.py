# ------------------------------------
q01 = {
"question": "Question_1",
"consigne": """
    Liste des employées qui ont menti sur leurs heures supplémentaires 
    et liste des employés ayant eu au moins une journée d'absence.
    Il ne doivent pas être pris en compte pour les questions de la première partie.""",
"variable": """
    Information pris en compte:
        - nb_jours_in_month = 31
        - nb_jours_férié = 1
        - nb_jours = nb_jours_in_month - nb_jours_férié
        - horaire_en_minute = (heure * 60) + minute
        - temps_de_travail_en_minute = horaire_depart - horaire_arrivee
        - triche = temps_de_travail_en_minute < (heure_sup * 60)""",
"justification": """
    Calcul des jours travailler par employée
        - en listant des personnes avec et sans absence
        - en compararant des personnes avec la liste des sans_absence
        - en vérifiant les heures sup erronée et non-erronée
    ['id', 'employee_name', 'department', 'location', 'date',
        'shift_start_time', 'shift_end_time', 'overtime_hours',
        'shift_start_minutes', 'shift_end_minutes', 'temps_de_travail_minutes',
        'overtime_minutes', 'break_duration', 'tricheurs_minutes',
        'nb_jours_travaillé', 'base', 'base_overtime', 'prime',
        'temps_effectif_hours', 'temps_hours&overtime', 'salaires_hours',
        'salaires_overtime', 'salaires']""",
"reponse": """
    Voici la liste des personnes qui n'ont pas menti sur leurs horaires"""}
# ------------------------------------
q02 = {
"question": "Question_2",
"consigne": """
    Liste des 10 employées qui coûtent le plus cher 
    (qui touche le plus gros salaire).""",
"variable": """
    salaire = {"Production": 12, "Management": 50, "Support": 20, "RD": 30}
    prime = {"Usine A": 15, "Usine B": 10, "Usine C": 20}""",
"justification": """
    Calcul du salaire de base
    Insertion du salaire de base en fonction du departement
    Insertion de la prime en fonction du departement
    Calcul du temps en heure
    Calcul du temps total travaillé en heure
    Calcul du salaire en fonction du temps de travail
    Calcul du salaire par jour
    Calcul du salaire par mois
    Insertion du salaire mensuel par employee
    Trie du dataframe par salaire""",
"reponse": """
    Voici la liste des 10er employés qui touchent le plus de salaire"""}
# ------------------------------------
q03 = {
"question": "Question_3",
"consigne": """
Liste des 10 employés qui ont travaillé le plus d'heure et leur salaire.""",
"variable": """

""",
"justification": """

""",
"reponse": """
    Voici """}
# ------------------------------------
q04 = {
"question": "Question_4",
"consigne": """
Liste des 10 employés qui ont travaillé le moins d'heure et leur salaire.""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------
q05 = {
"question": "Question_5",
"consigne": """
liste des 10 employés qui ont été le plus de temps en pause.""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------
q06 = {
"question": "Question_6",
"consigne": """
Liste des employées qui ont fait le plus des heures supplémentaires.""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------
q07 = {
"question": "Question_7",
"consigne": """
Répartition des jours de travail en fonction des usines.""",
"variable": """

    """,
"justification": """
""",
"reponse": """
    Voici """}
# ------------------------------------
q08 = {
"question": "Question_8",
"consigne": """
répartition des heures de travail en fonction des usines.""",
"variable": """
""",
"justification": """
""",
"reponse": """
    Voici """}
# ------------------------------------
q09 = {
"question": "Question_9",
"consigne": """
liste des 10 employés qui ont fait le plus de R&D par usine.""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------
q10 = {
"question": "Question_10",
"consigne": """
liste des 10 employés qui ont pris plus de 50 minutes de pause dans chaque usine.""",
"variable": """

    """,
"justification": """
""",
"reponse": """
    Voici"""}
# ------------------------------------
q11 = {
"question": "Question_11",
"consigne": """
liste des employés qui ont le plus travaillé par usine.""",
"variable": """
""",
"justification": """
""",
"reponse": """
    Voici """}
# ------------------------------------
q12 = {
"question": "Question_12",
"consigne": """
Combien d'heure supplémentaires ont été travaillées ?""",
"variable": """

""",
"justification": """

""",
"reponse": """
    Voici """}
# ------------------------------------
q13 = {
"question": "Question_13",
"consigne": """
Y a-t-il un lien entre heures supplémentaires et temps de pause ?""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------
q14 = {
"question": "Question_14",
"consigne": """
Y a-t-il un lien entre heures supplémentaires et heures travaillées ?""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici"""}
# ------------------------------------
q15 = {
"question": "Question_15",
"consigne": """
Y a-t-il un lien entre heures travaillées et temps de pause ?""",
"variable": """

    """,
"justification": """

    """,
"reponse": """
    Voici """}
# ------------------------------------