import pandas as pd


def extract_sheet_excel(path, sheet_index=0, header=0, col_index=None, skiprows=None):
    df = pd.read_excel(path, sheet_name=sheet_index, header=header, index_col=col_index, skiprows=skiprows)
    return df

def extract_all_same_sheet_excel(xls):
    """{"sheet": None, "dataframe": None, "statut_df": None}"""
    list_dico_df = []
    list_sheet = xls.sheet_names
    for sheet in list_sheet[1:]:
        # Création dictionnaire contenant dataframe for sheet
        dico_df = {}
        # Insertion jour du sheet
        dico_df["sheet"] = sheet
        # Insertion dataframe
        dataframe = extract_sheet_excel(xls, sheet, header=1)
        dico_df["dataframe"] = dataframe
        # Insertion Note sur le df en cours
        dico_df["statut_df"] = "RAS"
        # Insertion dico_df dans la liste_dico
        list_dico_df.append(dico_df)
    return list_dico_df

def list_duplicate(df):
    return df.drop_duplicates(keep = 'first', inplace=True)


if __name__ == "__main__":
    # Initialisation du fichier Excel
    path = 'DATA/employee.xlsx'
    xls = pd.ExcelFile(path)
    # Récupération du 1er sheet Employee
    dataframe_employee = extract_sheet_excel(xls, 0, header=0)
    # print(dataframe_employee.columns)
    dataframe_employee.drop(columns=dataframe_employee.columns[3:5], inplace=True)
    dataframe_employee.set_index(['employee_id'], inplace=True)   
    print("------Employee:------\n", dataframe_employee)
    # Récupération des autres sheets Journée_Travail par Date
    list_dico_dataframe = extract_all_same_sheet_excel(xls)
    for dico in list_dico_dataframe:
        print(dico["dataframe"].columns)
        dico["dataframe"].set_index(['id'], inplace=True)   
        print("------Jounée de Travail:", dico["sheet"],"------")
        print("------Dataframe:------\n", dico["dataframe"])
        df_test = dico["dataframe"]


    #     drop = list_duplicate(dataframe_employee)
    # print("Doublon:", drop)