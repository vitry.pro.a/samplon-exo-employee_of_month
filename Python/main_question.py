import pandas as pd
import time
import datetime
from fpdf import FPDF

from fonction_question import *

def main(path):
    file = "pointage.csv"
    file_full = "pointage_complet.csv"
    df_pointage = pd.read_csv(path + file)
    df_full = pd.read_csv(path + file_full)
    df_employee_Q1 = question_1(path, df_full)
    df_employee_Q2 = question_2(path, df_employee_Q1)
    question_3(path, df_employee_Q2)
    question_4(path, df_employee_Q2)
    question_5(path, df_employee_Q2)
    question_6(path, df_employee_Q2)
    df_employee_Q7 = question_7(path, df_employee_Q2)
    df_employee_Q8 = question_8(path, df_employee_Q7)
    question_9(path, df_employee_Q8)
    question_10(path, df_employee_Q8)
    question_11(path, df_employee_Q8)
    question_12(path, df_employee_Q8)
    question_13(path, df_employee_Q8)
    question_14(path, df_employee_Q8)
    question_15(path, df_employee_Q8)


if __name__ == "__main__":
    # Saisir le chemin du fichier (Mois) à analyser
    path = 'DATA/Export/'
    # Lancement du script
    print("------Lancement du script------")
    start = time.time()
    main(path)
    end = time.time()
    elapsed = end - start
    print(f'Temps d\'exécution : {elapsed:.2}ms')
    print("------Script terminé------")