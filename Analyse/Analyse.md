Le fichier `employee.xlsx` contient 2 parties:
 - La liste des employés avec leur id et leur departement
 - Les jours de travail pour chaque employé avec la location, leur pauses, les heures sup, et le debut/fin de leur shift

Du 1 janvier au 31 janvier, le fichier contient 1 jour férié (dimanche).
Les noms de colonne ne sont pas toutes homogènes, un formatage est nécessaire.

